<?php
# index.php
# osrs2go.com Homepage | Written by Roger Smiley
session_start();
require_once("vendor/autoload.php");
$fb = new Facebook\Facebook([
  'app_id' => '', // Replace {app-id} with your app id
  'app_secret' => '',
  'default_graph_version' => 'v2.2',
  ]);
use Mailgun\Mailgun;
$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR']; # Cloudflare Correction
require_once("inc/cms.class.php");
require_once("inc/user.class.php");
require_once("inc/server.class.php");
require_once("inc/credits.class.php");
require_once("inc/tickets.class.php");
require_once("inc/notifications.class.php");
#require( 'php_error.php' );
#    \php_error\reportErrors();
$settings['style'] = 'osrs2go';
$cms = new webcms();
$user = new user();
$servers = new servers();
$credits = new credits();
$tickets = new tickets();
$notify = new notify();

# Crons
$servers->runCron();
$credits->processPaymentQueue();

# This is the file based page system
    switch(key($_GET)) {
    	case 'new-server':
      $page = 'newServer.php';
      break;
      case 'ping-server':
      DB::update('servers', array(
        'pinged' => 1,
        'pingreceived' => time()
        ), "serverip = %s AND running = 1", $_SERVER['REMOTE_ADDR']);
      die('IP Recorded: '.$_SERVER['REMOTE_ADDR']);
      break;
      case 'force-logout':
      $page = 'forceLogout.php';
      break;
      case 'paypal':
      $credits->acceptPayment(json_encode($_REQUEST));
      break;
      case 'new-ticket':
      $page = 'newTicket.php';
      break;
      case 'show-ticket':
      $page = 'showTicket.php';
      break;
      case 'support':
      $page = 'tickets.php';
      break;

      # Functional Pages #
      case 'get-user-json':
      echo $user->getAllUsersAndID();
      exit();
      break;
      case 'restart':
      $page = 'restart.php';
      break;
      case 'extend':
      $page = 'extend.php';
      break;
      # End Functional Pages #

      default:
      $page = 'home.php';
      break;
    }

    if($user->isLoggedIn()) {
      if($user->isAdmin($_SESSION['userid'])) {
        switch(key($_GET)) {
          case 'new-page':
          $page = 'admin/newpage.php';
          break;
          case 'edit-page':
          $page = 'admin/editpage.php';
          break;
          case 'page-manager':
          $page = 'admin/pages.php';
          break;
          case 'servers':
          $page = 'admin/stats.php';
          break;
          
          case 'users':
          $page = 'admin/users.php';
          break;
              case 'manage-user':
              $page = 'admin/userManage.php';
              break;

          case 'support-admin':
          $page = 'admin/tickets.php';
          break;

          /* Admin Panel Linking */
          case 'admin':
          $cms->adminWorld();
          $page = 'admin/full/panel.php';
          break;
          case 'userAdmin':
          $cms->adminWorld();
          $page = 'admin/full/user.php';
          break;
        }
      }
    }
# This is the file based page system


# If database is active, this is the dynamic page system
if($page == 'home.php' && ($cms->dbactive() === TRUE && $cms->pageExist(key($_GET)))) {
  # The logic behind this is that if $page is still home, they're either hpmepage or db driven
  #$cms->adminWorld();
  $page = 'temp.php';
}
# If database is active, this is the dynamic page system


###########################################################
# Below will house the actual construction of the page    #
###########################################################

if(file_exists('style/'.$settings['style'].'/header.php')) {
  require_once("style/{$settings['style']}/header.php");
} else {
  die('Style header is invalid, please be sure the headers named <b>header.php</b>');
}

if(file_exists("pages/{$page}")) {
  require_once("pages/{$page}");
} else {
  create_error('<b>We could not find the file you are looking for, I wonder if it even exists.</b>');
}
if(file_exists('style/'.$settings['style'].'/footer.php')) {
  require_once("style/{$settings['style']}/footer.php");
} else {
  die('Style footer is invalid, please be sure the footer named <b>footer.php</b>');
}
?>
