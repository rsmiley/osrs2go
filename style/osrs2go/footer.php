<?php
 $base_url = 'https://osrs2go.com/style/osrs2go';
?>
<footer class="footer">

    <div class="container-fluid">

        <nav class="pull-left">

            <ul>



                <li>

                    <a href="https://www.facebook.com/groups/OldSchool.Runescape.OSRS/">

                        OSRS Facebook Group

                    </a>

                </li>

                <li>

                    <a href="http://oldschool.runescape.com">

                       Oldschool Runescape

                    </a>

                </li>

            </ul>

        </nav>

        <div class="copyright pull-right">

            &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="mailto:osrs2go@mailhub.co">Roger Smiley</a><br /><small>By using our services, you agree to the <a href='/aup'>Acceptable Use Policy</a></small>
<!-- http://www.creative-tim.com | I appreciate it broski, however I don't want a public copyright right now. I'll pay you for the remove soon, I promise! -->
        </div>

    </div>

</footer>



</div>

</div>





</body>



<!--   Core JS Files   -->

<script src="<?php echo $base_url; ?>/assets/js/jquery-1.10.2.js?v=1" type="text/javascript"></script>


<script src="<?php echo $base_url; ?>/assets/js/bootstrap.min.js?v=1" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>



<!--  Checkbox, Radio & Switch Plugins -->

<script src="<?php echo $base_url; ?>/assets/js/bootstrap-checkbox-radio.js?v=1"></script>


<!--  Notifications Plugin    -->

<script src="<?php echo $base_url; ?>/assets/js/bootstrap-notify.js?v=1"></script>


<!-- Paper Dashboard Core javascript and methods for Demo purpose -->

<script src="<?php echo $base_url; ?>/assets/js/paper-dashboard.js?v=1"></script>

<? /*
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->

<script src="<?php echo $base_url; ?>/assets/js/demo.js?v=1"></script>



<script type="text/javascript">

$(document).ready(function(){
  $.notify({
      icon: 'ti-gift',
      message: "Welcome to <b>osrs2go</b> - this is a development zone."
    },{
        type: 'warning',
        timer: 600
    });

});

</script>
*/ 
?>


</html>

