<!doctype html>
<?php
 $base_url = 'https://osrs2go.com/style/'.basename(dirname(__FILE__));

 $title = str_replace('-', ' ', key($_GET));
 if($title == '') {
    $title = 'Home';
 }
 if($title == 'aup') {
    $title = 'Acceptable Use Policy';
 }
 if($title == 'faq') {
    $title = 'Frequently Asked Questions';
 }
 $title = ucwords($title);
 /*
     <script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '665a90ab2e31c56df4b495774c59f139c202ac71');
    <?php if($user->isLoggedIn()) { ?>
        smartlook('tag', 'name', '<?php echo $user->getRealName($_SESSION['userid']); ?>');
      <?php } ?>

    </script>
    */
 ?>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $base_url; ?>/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $base_url; ?>/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-6127573-4', 'auto');
      <?php if($user->isLoggedIn()) { ?>
      ga('set', 'userId', <?php echo $_SESSION['userid']; ?>); 
      <?php } ?>
      ga('send', 'pageview');
    </script>

	<title>osrs2go | <?php echo $title; ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo $base_url; ?>/assets/css/bootstrap.min.css?v=1" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo $base_url; ?>/assets/css/animate.min.css?v=1" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo $base_url; ?>/assets/css/paper-dashboard.css?v=1" rel="stylesheet"/>
    <link href="<?php echo $base_url; ?>/assets/css/demo.css?v=1" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css?v=1" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo $base_url; ?>/assets/css/themify-icons.css?v=1" rel="stylesheet"> 
    </head>
<body>
<?php
function base64UrlEncode($_input)
{
    return str_replace(array('=','+','/'),array('_','-',','),base64_encode($_input));
}

if($user->isLoggedIn()) {
    $liveChatUser = $user->getRealName($_SESSION['userid']);
} else {
    $liveChatUser = "Guest User";
}
?>
<!-- livezilla.net PLACE IN BODY --><div id="lvztr_517" style="display:none"></div><script id="lz_r_scr_e1af1264168c28cb876d183e33cf921c" type="text/javascript">lz_ovlel = [{type:"wm",icon:"commenting"},{type:"chat",icon:"comments",counter:true},{type:"facebook",icon:"facebook",color:"#3b5998",margin:[0,0,20,0],href:"aHR0cHM6Ly9mYi5jb20vb3NyczJnbw__"}];lz_ovlec = null;lz_code_id="e1af1264168c28cb876d183e33cf921c";var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "https://chat.osrs2go.com/server.php?rqst=track&output=jcrpt&en=<?php echo base64UrlEncode($liveChatUser); ?>&mp=MQ__&ovlv=djI_&ovltwo=MQ__&ovlc=MQ__&esc=IzQwNzhjNw__&epc=IzQ5ODllMQ__&ovlts=MA__&ovloo=MQ__&ovlapo=MQ__&nse="+Math.random();script.src=src;document.getElementById('lvztr_517').appendChild(script);</script><noscript><img src="https://chat.osrs2go.com/server.php&quest;rqst=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript><!-- http://www.livezilla.net -->

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="/" class="simple-text">
                    osrs2go
                </a>
            </div>
            <?php
            if(preg_match("/getting-started/", $_SERVER['REQUEST_URI'])) {
                $ua = ' class="active"';
            }elseif(preg_match("/credits/", $_SERVER['REQUEST_URI'])) {
                $uc =  ' class="active"';
            }elseif(preg_match("/support/", $_SERVER['REQUEST_URI'])) {
                $us =  ' class="active"';
            } elseif(preg_match("/servers/", $_SERVER['REQUEST_URI'])) {
                $use =  ' class="active"';
            } elseif(preg_match("/faq/", $_SERVER['REQUEST_URI'])) {
                $fa =  ' class="active"';
            } elseif(preg_match("/common-problems/", $_SERVER['REQUEST_URI'])) {
                $cps =  ' class="active"';
            } elseif(preg_match("/feedback/", $_SERVER['REQUEST_URI'])) {
                $ts =  ' class="active"';
            } else {
                $dash =  ' class="active"';
            }
            ?>

            <ul class="nav">
                <li<?php echo $dash; ?>>
                    <a href="/">
                        <i class="ti-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <!-- <li<?php echo $use; ?>>
                    <a href="/servers/">
                        <i class="ti-panel"></i>
                        <p>Server Manager</p>
                    </a>
                </li> -->
                <li<?php echo $uc; ?>>
                    <a href="/credits/">
                        <i class="ti-money"></i>
                        <p>Buy Credits</p>
                    </a>
                </li>
                <li<?php echo $ua; ?>>
                    <a href="/getting-started/">
                        <i class="ti-user"></i>
                        <p>Getting Started</p>
                    </a>
                </li>
                <li<?php echo $fa; ?>>
                    <a href="/faq/">
                        <i class="ti-search"></i>
                        <p>FAQ</p>
                    </a>
                </li>
                
                <li<?php echo $cps; ?>>
                    <a href="/common-problems/">
                        <i class="ti-ruler-pencil"></i>
                        <p>Common Problems</p>
                    </a>
                </li>
                <li<?php echo $us; ?>>
                    <a href="/support/">
                        <i class="ti-email"></i>
                        <p>Support Desk</p>
                    </a>
                </li>
                <?php
                if($user->isLoggedIn()) {
                    if($user->isAdmin($_SESSION['userid'])) {
                        ?>
                <hr>
                        <li>
                        <a href="/admin/">
                            <i class="ti-wheelchair"></i>
                            <p>Admin Panel</p>
                        </a>
                            </li>
                        <li>
                    <a href="/page-manager/">
                        <i class="ti-layout"></i>
                        <p>Page Manager</p>
                    </a>
                        </li>
                        <li>
                    <a href="/users/">
                        <i class="ti-wheelchair"></i>
                        <p>User List</p>
                    </a>
                        </li>
                        <li<?php echo $use; ?>>
                    <a href="/servers/">
                        <i class="ti-server"></i>
                        <p>Servers</p>
                    </a>
                        </li>
                        <li<?php echo $us; ?>>
                    <a href="/support-admin/">
                        <i class="ti-email"></i>
                        <p>Support Admin</p>
                    </a>
                </li>
                        <?
                    }
                }
                ?>
            </ul>
    	</div>
    </div>

<?php include "top-bar.php"; ?>


        <div class="content">
            <div class="container-fluid">