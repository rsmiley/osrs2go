<div class="main-panel">

    <nav class="navbar navbar-default">

        <div class="container-fluid">

            <div class="navbar-header">

                <button type="button" class="navbar-toggle">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar bar1"></span>

                    <span class="icon-bar bar2"></span>

                    <span class="icon-bar bar3"></span>

                </button>

                <a class="navbar-brand" href="#"></a>

            </div>

            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                            <?php
                            if($user->isLoggedIn()) {
                            $c = $notify->getUserNotify($_SESSION['userid']);
                            $d = count($c);
                            foreach($c as $k) {
                                $replytime = date('F j, Y', $k['time']).' at '.date('g:i a', $k['time']);
                                $echoNotify .= "<li><a href='#'>{$k['message']}<br />
                                <small class='pull-right'>$replytime</small><br /></a></li>";
                                if($k['message'] == 'You have no notifications') {
                                    $d = 0;
                                }
                            }
                          } else {
                            $echoNotify = '<li><a href="#"><small><b>Login to get started!</b></small></a></li>';
                          }
                          ?>
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <i class="ti-bell"></i>

                                <p class="notification"><?php echo $d; ?></p>

              <p>Notifications</p>

              <b class="caret"></b>

                          </a>

                          <ul class="dropdown-menu">
                          <?php
                          echo $echoNotify;
                          ?>
                          </ul>

                    </li>

                </ul>



            </div>
        </div>

    </nav>

