<?php
 $base_url = 'https://osrs2go.com/style/'.basename(dirname(__FILE__)).'/';
 #if(isset($_POST[]))
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

<title>osrs2go Admin</title>

<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="<?php echo $base_url; ?>css/bootstrap.min.css" />

<link rel="stylesheet" href="<?php echo $base_url; ?>css/bootstrap-responsive.min.css" />

<link rel="stylesheet" href="<?php echo $base_url; ?>css/maruti-style.css" />

<link rel="stylesheet" href="<?php echo $base_url; ?>css/maruti-media.css" class="skin-color" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />


</head>

<body>



<!--Header-part-->

<div id="header">

  <h1><a href="<?php echo $base_url; ?>dashboard.html">Maruti Admin</a></h1>

</div>

<!--close-Header-part--> 

<!--top-Header-menu-->

<div id="user-nav" class="navbar navbar-inverse">

  <ul class="nav">

    <li class="" ><a title="" href="<?php echo $base_url; ?>#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>

    <li class=" dropdown" id="menu-messages"><a href="<?php echo $base_url; ?>#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>

      <ul class="dropdown-menu">

        <li><a class="sAdd" title="" href="<?php echo $base_url; ?>#">new message</a></li>

        <li><a class="sInbox" title="" href="<?php echo $base_url; ?>#">inbox</a></li>

        <li><a class="sOutbox" title="" href="<?php echo $base_url; ?>#">outbox</a></li>

        <li><a class="sTrash" title="" href="<?php echo $base_url; ?>#">trash</a></li>

      </ul>

    </li>

    <li class=""><a title="" href="<?php echo $base_url; ?>#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>

    <li class=""><a title="" href="<?php echo $base_url; ?>login.html"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>

  </ul>

</div>

<div id="search">
<form action='/userAdmin' method='get'>
  <input type="text" name='userSearch' autocomplete='off' placeholder="Lookup user here...">

  <button type="submit" class="tip-left" title="Search"><i class="icon-search icon-white"></i></button>
</form>

</div>

<!--close-top-Header-menu-->

<?php
include('sidebar.php');
?>