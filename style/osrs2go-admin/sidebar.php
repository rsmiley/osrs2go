<div id="sidebar"><a href="#" class="visible-phone"><i class="fa fa-home"></i> Open Menu</a><ul>

    <li class="active"><a href="/admin/"><i class="fa fa-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="#"><i class="fa fa-drivers-license-o"></i> <span>User Manager</span></a> </li>
    <li><a href="#"><i class="fa fa-microchip"></i> <span>Server Manager</span></a> </li>
    <li><a href="#"><i class="fa fa-sitemap"></i> <span>API Settings</span></a> </li>
    <li><a href="#"><i class="fa fa-sliders"></i> <span>System Settings</span></a> </li>
    <li class="submenu"> <a href="#"><i class="fa fa-file"></i> <span>Alerts and Information</span> <span class="label label-important">0</span></a>
      <ul>
        <li><a href="#">System Alerts</a></li>
        <li><a href="#">Google API Info</a></li>
        <li><a href="#">Facebook Errors</a></li>
        <li><a href="#">Error Logs</a></li>
      </ul>
<?php /*
    <li> <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li>

    <li> <a href="widgets.html"><i class="icon icon-inbox"></i> <span>Widgets</span></a> </li>

    <li><a href="tables.html"><i class="icon icon-th"></i> <span>Tables</span></a></li>

    <li><a href="grid.html"><i class="icon icon-fullscreen"></i> <span>Full width</span></a></li>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <span class="label label-important">3</span></a>

      <ul>

        <li><a href="form-common.html">Basic Form</a></li>

        <li><a href="form-validation.html">Form with Validation</a></li>

        <li><a href="form-wizard.html">Form with Wizard</a></li>

      </ul>

    </li>

    <li><a href="buttons.html"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>

    <li><a href="interface.html"><i class="icon icon-pencil"></i> <span>Eelements</span></a></li>

    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Addons</span> <span class="label label-important">4</span></a>

      <ul>

        <li><a href="index2.html">Dashboard2</a></li>

        <li><a href="gallery.html">Gallery</a></li>

        <li><a href="calendar.html">Calendar</a></li>

        <li><a href="chat.html">Chat option</a></li>

      </ul>
      */ ?>

    </li>

  </ul>

</div>
