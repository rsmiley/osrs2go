<?php
		
include('inc/cli/Net/SSH2.php');
class servers extends webcms {
	function __construct() {
		putenv('GOOGLE_APPLICATION_CREDENTIALS=/home/osrs2go/service-account.json');
		$gg = new Google_Client();
		$gg->setApplicationName('osrs2go Server Manager');
		$gg->useApplicationDefaultCredentials();
		$gg->addScope('https://www.googleapis.com/auth/cloud-platform');
		$this->gcloud = new Google_Service_Compute($gg);
		$this->ssh = new Net_SSH2('localhost');
		if (!$this->ssh->login('root', '')) {
		    exit('Failed to connect to the CLI');
		}
	$this->dns = new Cloudflare\Zone\Dns('email', 'api-key');
  }

  	function extendServer($server) {
  		if(!user::isLoggedIn()) { return webcms::errorPage("You must be logged in to launch a server."); }
  		$serverInfo = $this->getServer($server);
  		if(is_null($serverInfo)) { return webcms::errorPage("The server you've requested to extend no longer exists."); }
  		if($serverInfo['running'] == 0) { return webcms::errorPage("We cannot extend a server that's not running."); }
  		if(user::getCredits($serverInfo['owner']) < $serverInfo['level']) { return webcms::errorPage("You cannot afford to extend your server."); }

  			$this->chargeServer($_SESSION['userid'], $serverInfo['level']);
  			DB::insert('transactionLog', array(
		    'user' => $_SESSION['userid'],
		    'serverid' => "EXTENTION: {$serverInfo['servername']}",
		    'amount' => $serverInfo['level'],
		    'time' => time()
		  	));
  			DB::update('servers', array(
  				'reqhours' => $serverInfo['reqhours'] + 3
  				), "servername = %s", $serverInfo['servername']);
  		return webcms::successMessage("You've added 3 extra hours to your server!");
  	}

  	function restartServer($serverid) {
  		$project = 'noble-radio-152709';
  		$restartthis = $this->getServer($serverid);
  		if(!is_array($restartthis)) { webcms::errorPage("The requested server does not exist."); }

		$response = $this->gcloud->instances->get($project, $restartthis['zone'], $restartthis['servername']);
		$response = $this->objectToArray($response);
		if($response['status'] != 'TERMINATED') { return webcms::errorPage("The server is already online or in the process of booting.<br /><br />Returned Status: {$response['status']}"); }
  		try {
			$response2 = $this->gcloud->instances->start($project, $restartthis['zone'], $restartthis['servername']);
			echo "Please wait while your server restarts...<br />";
			sleep(10); # Taking a break for IP to allocate
			
				# Getting the server IP to update the DNS #
			$getIP = $this->gcloud->instances->get($project, $restartthis['zone'], $restartthis['servername']);
			$getIParray = $this->objectToArray($getIP);
			$yes = str_replace("\u0000*\u0000", '', json_encode($getIParray));
			$kay = json_decode($yes, true);
			if($kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP'] == NULL) {
				$this->logError("Server restarted with no IP", user::getRealName($_SESSION['userid']));
				return webcms::errorPage("We failed to get an IP while rebooting.<br />Please submit a support ticket for a credit refund.");
			} else {
				# Update the DNS entry with the new IP.
				$cF = $this->dns->update('api-key', $restartthis['quickdns-id'], 'A', $restartthis['quickdns'], $kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP']);				
				return webcms::successMessage("Server restarted successfully!<br />Please wait up to 5 minutes before trying to connect");
			}
			} catch (Exception $e) {
				return webcms::errorPage("An error occured while trying to restart your server.");
			}
  	}

	function chargeServer($user, $level) {
		# Reduce the credits and associate the charge with the server id
		if(!user::userExists($user)) { return webcms::errorPage("Unable to charge for the server"); }
		if($level >= 4 || $level < 1) { return webcms::errorPage("Nope."); }
		user::updateUser($user, array('credits'), array(user::getCredits($user)-$level));
		
		#return webcms::errorPage("Successfully charged you for the requested server");
	}

	function refundServer($user, $level) {
		# We didnt get an IP or something went wrong. Refund.
		# Try to start a new google class here ?
		user::updateUser($user, array('credits'), array(user::getCredits($user)+$level));
		DB::insert('transactionLog', array(
		    'user' => $user,
		    #'serverid' => %SERVER_NAME%,
		    'amount' => -$level,
		    'time' => time()
		  ));
	}

	function launchServer($user, $level, $zone) {
		if(!user::isLoggedIn()) { return webcms::errorPage("You must be logged in to launch a server."); }
		if(!user::userExists($user)) { return webcms::errorPage("Unable to charge for the server"); }
		if(!user::hasReadGettingStarted($_SESSION['userid'])) { return webcms::errorPage("You must read the <a href='/getting-started/'>Getting Started</a> page before you can start a server!"); }
		if(user::isBanned($_SESSION['userid'])) { return webcms::errorPage("You are banned from using our services.<br />Please submit a support ticket for further support."); }
		if($this->slowDown($user) && !user::isAdmin($_SESSION['userid'])) { return webcms::errorPage("<b>Woah! Slow down!</b><br />You have already started a server in the last 5 minutes!<br /><br />Find your server info under your name!"); }
		if($level >= 4 || $level < 1) { return webcms::errorPage("Nope."); }
		if(empty($zone) || $zone == 0) { return webcms::errorPage("There's no zone selected for this server."); }
		#if(count(json_decode($this->getRunning(), true)) >= 50) { return webcms::errorPage("There are to many servers currently running.<br />Please wait for the next available spot!"); }
		# ^ Code is outdated as we now have the ability to set a real limit for a zone.

		#if(!user::isAdmin($_SESSION['userid'])) {
		#	return webcms::errorPage("Due to a known issue with our provider, we are currently offline. We will return as soon as possible!");
		#}

		# Zone Check
		$selectZone = DB::queryFirstRow("SELECT * FROM zonelimits WHERE id = %i", $zone);
		$getZones = $this->zoneCheck();
		if($getZones[$selectZone['zone']] <= 0) {
			$this->logError("Zone ({$selectZone['zone']}) was full - unable to start", $user);
			return webcms::errorPage("This zone is full.");
		}

		$credits = user::getCredits($user);
		if($credits < $level) {
			return webcms::errorPage("You can't afford this.");
		}
		

		
		$project = 'noble-radio-152709';
		$zone = $this->getZoneName($zone);
		$ran = 'osrs2go-'.time();
		$this->chargeServer($user, $level);
		DB::insert('transactionLog', array(
		    'user' => $user,
		    'serverid' => $ran,
		    'amount' => $level,
		    'time' => time()
		  ));
		switch($level) {
			case '1':
			$lvl = '1';
			break;
			case '2':
			$lvl = '2';
			break;
			case '3':
			$lvl = '4';
			break;
			default:
			$lvl = '1';
			break;
		}
		$template = 'gcloud compute --project "noble-radio-152709" instances create "'.$ran.'" --zone "'.$zone.'" --machine-type "n1-standard-'.$lvl.'" --subnet "default" --no-restart-on-failure --maintenance-policy "TERMINATE" --preemptible --scopes 788913449155-compute@developer.gserviceaccount.com="https://www.googleapis.com/auth/cloud-platform" --tags "http-server","https-server" --image "osrs2go" --image-project "noble-radio-152709" --boot-disk-size "50" --boot-disk-type "pd-standard" --boot-disk-device-name "'.$ran.'"';
		#echo $template;
		$dd = $this->ssh->exec($template);
		echo "Please wait while we start your server... <br /><br />";
		
		sleep(6);
		try {
		$response = $this->gcloud->instances->get($project, $zone, $ran);
		$a = $this->objectToArray($response); 
		} catch (Exception $e) {
		$this->refundServer($user, $level);
		$this->logError("Server failed to start [Zone: $zone]", $user);
		webcms::errorPage("We encountered an error while trying to launch your server. Please try again!<br /><br />If this is a reoccuring error,<br /> Email <a href='mailto:osrs2go@mailhub.co'>osrs2go@mailhub.co</a> for fast support!");
		}

		$yes = str_replace("\u0000*\u0000", '', json_encode($a));
		$kay = json_decode($yes, true);
		if($kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP'] == NULL) {
			# WE DID NOT GET AN IP. ABORT ABORT
		$response = $this->gcloud->instances->delete($project, $zone, $ran);
		$this->refundServer($user, $level);
		$this->logError("Server failed to obtain IP address.", $user);
		return parent::errorPage("Sometimes we have trouble getting a spot in line!<br />Please try requesting another server!<br /><br />If this is a reoccuring error,<br /> Email <a href='mailto:osrs2go@mailhub.co'>osrs2go@mailhub.co</a> for fast support!");
		}
    	$qeb = explode('.', $kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP']);
    	$qname = file_get_contents('http://www.setgetgo.com/randomword/get.php?len=6').'-'.$qeb['3'];
    	$cfAutoDNS = $this->dns->create('api-key', 'A', $qname, $kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP'], 120);
    	$cfAutoDNS = $this->objectToArray($cfAutoDNS);
		DB::insert('servers', array(
	    'owner' => $user,
	    'started' => $this->onlytime($ran),
	    'running' => 1,
	    'serverip' => $kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP'],
	    'ownerip' => $_SERVER['REMOTE_ADDR'],
	    'zone' => $zone, 
	    'servername' => $a['name'],
	    'serverid' => $a['id'],
	    'reqhours' => 3,
	    'level' => $level,
	    'fulldata' => $yes,
	    'quickdns' => $qname,
	    'quickdns-id' => $cfAutoDNS['result']['id']
	  ));
		DB::update('users', array(
	    'lastinstanceid' => DB::insertId(),
	    'laststart' => $this->onlytime($ran),
   		'lastknownip' => $_SERVER['REMOTE_ADDR']), "fbid=%i", $user);
		/*ID: {$a['id']} <br />
		Name: {$a['name']}<br />
		IP: {$kay["modelData"]['networkInterfaces'][0]['accessConfigs'][0]['natIP']}<br />*/
		notify::newNotify($user, "<b>New Server Started!</b><br />
								Server: $qname.osrs2go.com<br />
								User: Runescape<br />
								Pass: W3lcome!");
		print "Thanks for starting up a server! It's now ready for you to log in!<br /><br />
		Server Address: <b>{$qname}.osrs2go.com</b><br />
		Username: <b>Runescape</b><br />
		Password: <b>W3lcome!</b><br /><br />
		Once you login, please hit CTRL+ALT+DELETE and change your password. You don't <b>have to do it</b>, but just in case. I seriously recommend doing so until autogeneration is readded.

		<br ><p><b>Note: Sometimes it takes an extra minute or 3 to boot up! Just keep trying to connect :)</b></p>";
	}

	function runCron() {
		# Search through all active servers and send a kill list to the API
		$project = 'noble-radio-152709';
		$d = $this->getRunning();
		$d = json_decode($d, true);
		foreach($d as $k) {
			if($this->expectedDeath($k['servername']) < time()) {
			$cF = $this->dns->delete_record('api-key', $k['quickdns-id']);
			$response = @$this->gcloud->instances->delete($project, $k['zone'], $k['servername']);
			DB::update('servers', array('running' => 0,), "serverid=%i", $k['serverid']);
			}
		}
	}

	function slowDown($user) {
		$t = strtotime('+5 minutes', $this->getUserLatestServer($user));
		if($t >= time()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function zoneCheck($where='') {
		$y = $this->getRunning();
		$ye = json_decode($y, true);
		$central = 0; $west = 0; $east = 0; $europewest = 0; $asiaeast = 0; $asianortheast = 0; $asiasoutheast = 0; $virginia = 0; $london = 0; $australia = 0; # Set them all to 0 just in case.
		foreach($ye as $g) {
			if(preg_match("/us-central1/", $g['zone'])) {
				$central++;
			}
			if(preg_match("/us-west1/", $g['zone'])) {
				$west++;
			}
			if(preg_match("/us-east1/", $g['zone'])) {
				$east++;
			}
			if(preg_match("/us-east4/", $g['zone'])) {
				$virginia++;
			}
			if(preg_match("/europe-west1/", $g['zone'])) {
				$europewest++;
			}
			if(preg_match("/europe-west2/", $g['zone'])) {
				$london++;
			}
			if(preg_match("/asia-east1/", $g['zone'])) {
				$asiaeast++;
			}
			if(preg_match("/asia-northeast1/", $g['zone'])) {
				$asianortheast++;
			}
			if(preg_match("/asia-southeast1/", $g['zone'])) {
				$asiasoutheast++;
			}
			if(preg_match("/australia-southeast1/", $g['zone'])) {
				$australia++;
			}	
		}
		# Get the limits
		$zl = DB::query("SELECT * FROM zonelimits ORDER BY orderid ASC");
		foreach($zl as $k) {
			switch($k['zone']) {
				case 'central':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $central;
				break;
				case 'east':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $east;
				break;
				case 'west':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $west;
				break;
				case 'europe-west':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $europewest;
				break;
				case 'asia-east':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $asiaeast;
				break;
				case 'asia-northeast':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $asianortheast;
				break;
				case 'asia-southeast':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $asiasoutheast;
				break;
				case 'virginia':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $virginia;
				break;
				case 'london':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $london;
				break;
				case 'australia':
				$remainingAvailable[$k['zone']] = $k['zonelimit'] - $australia;
				break;
			}
			}
			return $remainingAvailable;
		}

	function getZoneName($zoneid) {
		switch($zoneid) {
			case '1':
				$rand = mt_rand(1,2);
				switch($rand) {
					case '1':
					$zone = 'us-west1-a';
					break;
					case '2':
					$zone = 'us-west1-b';
					break;
				}
			break;
			case '2':
			$rand = mt_rand(2,4); # I took zone 1 out on 8-22-17 due to sleepy machines
				switch($rand) {
					case '1':
					$zone = 'us-central1-a';
					break;
					case '2':
					$zone = 'us-central1-b';
					break;
					case '3':
					$zone = 'us-central1-c';
					break;
					case '4':
					$zone = 'us-central1-f';
					break;
				}
			break;
			case '3':
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'us-east1-b';
					break;
					case '2':
					$zone = 'us-east1-c';
					break;
					case '3':
					$zone = 'us-east1-d';
					break;
				}
			break;
			case '4':
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'europe-west1-b';
					break;
					case '2':
					$zone = 'europe-west1-c';
					break;
					case '3':
					$zone = 'europe-west1-d';
					break;
				}
			break;
			case '5':
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'asia-east1-a';
					break;
					case '2':
					$zone = 'asia-east1-b';
					break;
					case '3':
					$zone = 'asia-east1-c';
					break;
				}
			break;
			case '6':
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'asia-northeast1-a';
					break;
					case '2':
					$zone = 'asia-northeast1-b';
					break;
					case '3':
					$zone = 'asia-northeast1-c';
					break;
				}
			break;
			case '7':
			$rand = mt_rand(1,2);
				switch($rand) {
					case '1':
					$zone = 'asia-southeast1-a';
					break;
					case '2':
					$zone = 'asia-southeast1-b';
					break;
				}
			break;
			case '8': # Virginia
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'us-east4-a';
					break;
					case '2':
					$zone = 'us-east4-b';
					break;
					case '3':
					$zone = 'us-east4-c';
					break;
				}
			break;
			case '9': # London
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'europe-west2-a';
					break;
					case '2':
					$zone = 'europe-west2-b';
					break;
					case '3':
					$zone = 'europe-west2-c';
					break;
				}
			break;
			case '10': #	Sydney, Australia
			$rand = mt_rand(1,3);
				switch($rand) {
					case '1':
					$zone = 'australia-southeast1-a';
					break;
					case '2':
					$zone = 'australia-southeast1-b';
					break;
					case '3':
					$zone = 'australia-southeast1-c';
					break;
				}
			break;
			default:
			$zone = 'us-central1-a';
			break;
		}
		return $zone;
	}

	function logError($error, $user) {
		DB::insert('serverErrorLog', array(
			'time' => time(),
			'user' => user::getRealName($user),
			'error' => $error
			));
	}

	function getRunning() {
		# Get running servers
		$l = DB::query("SELECT * FROM servers WHERE running = %i", 1);
		$r = array();
		foreach($l as $k => $v) {
			array_push($r, $v);
		}
		return json_encode($r);
	}

	function getUserRunning($user) {
		# Get the running servers for a user.
		$l = DB::query("SELECT * FROM servers WHERE running = %i AND owner = %i", 1, $user);
		$r = array();
		if(!is_null($l)) {
		foreach($l as $k => $v) {
			array_push($r, $v);
		}
		return json_encode($r);
		} else {
			return FALSE;
		}
	}

	function getUserLatestServer($user) {
		$l = DB::queryFirstRow("SELECT * FROM servers WHERE running = %i AND owner = %i ORDER BY started DESC", 1, $user);
		return $l['started'];
	}

	function getServer($server) {
		$l = DB::queryFirstRow("SELECT * FROM servers WHERE servername = %s ORDER BY started DESC", $server);
		return $l;
	}

	function getAllServers() {
		# We want all, not just the running
		$l = DB::query("SELECT * FROM servers");
		$r = array();
		foreach($l as $k => $v) {
			array_push($r, $v);
		}
		return json_encode($r);
	}

	function getAllUsersServers($user) {
		# Lets look at them all!
		$l = DB::query("SELECT * FROM servers WHERE owner = %i ORDER BY id DESC", $user);
		$r = array();
		foreach($l as $k => $v) {
			array_push($r, $v);
		}
		return json_encode($r);
	}

	function getStartTime($server) {
		# When did the requested server start?
		$l = DB::queryFirstRow("SELECT started FROM servers WHERE id = %i OR serverid = %s", $server, $server);
		return $l['started'];
	}

	function expectedDeath($server) {
		# For ease of cron writing and displaying on end-user. Let's do the strtotime('3 hours') here.
		$serv = $this->getServer($server);
		return strtotime('+'.$serv['reqhours'].' hours', $serv['started']);
	}




	function objectToArray ($object) {
    if(!is_object($object) && !is_array($object))
        return $object;

    return array_map('self::objectToArray', (array) $object);
	}
	function onlytime($i) { $i = str_replace('osrs2go-', '', $i); return $i; }
}

?>