<?php

class webcms {

  function __construct() {
    if(file_exists('inc/db.php')) {
    include "inc/db.php";
      if($dbCon['enabled'] === TRUE) {
        include "inc/dbClass.php";
        new DB();
        DB::$user = $dbCon['username'];
        DB::$password = $dbCon['password'];
        DB::$dbName = $dbCon['dbname'];
        #if(class_exists('DB')) { echo "DB is active <br />"; }
      } else {
        # We have database connection details, but they're not enabled.
        #echo "Database Connection details exist, but they're not enabled";
        return FALSE;
      }
    } else {
      # We have no database connection details, no sense moving forward
      #echo "Database is disabled";
      return FALSE;
    }

    include "inc/paypal.ipn.php";
    # Lets not forget to set the fucking time zone
    date_default_timezone_set('America/Phoenix');
  }

  function dbactive() {
    if(class_exists('DB')) { return TRUE; } else { return FALSE; }
  }

  function adminWorld() {
    if($this->dbactive()) {
      global $settings;
      $settings['style'] = 'osrs2go-admin';
    } else {
      # set a page to alert that there's no admin panel without a database
      $this->errorPage('There can be no admin panel with no database connection.');
    }
  }

  function errorPage($msg) {
    global $settings;
    $_SESSION['error_message'] = $msg;
    require_once('pages/error.php');
    require_once('style/osrs2go/footer.php');
    exit();
  }

  function test() {
    $this->getPage(1);
  }
  
  function pageExist($id) {
     $ck = DB::query("SELECT * FROM `pages` WHERE `id` = %i or `crumb` = %s", $id, $id);
    if(count($ck) == 0) { return FALSE; } else { return TRUE; }
  }
  
  function getPage($id) {
    if(!$this->dbactive()) { $this->errorPage('No active database'); }
    if(!$this->pageExist($id)) { $this->errorPage('Page does not exist'); } else {
         $ck = DB::query("SELECT * FROM `pages` WHERE `id` = %i or `crumb` = %s", $id, $id);
          return $ck;
    }
  }
  function pageLoginOnly($id) {
    if(!$this->dbactive()) { $this->errorPage('No active database'); }
    if(!$this->pageExist($id)) { $this->errorPage('Page does not exist'); } else {
      $q = DB::queryFirstRow("SELECT `loggedinonly` FROM pages WHERE `id` = %i or `crumb` = %s", $id, $id);
        if($q['loggedinonly'] == 1) {
          return TRUE;
        } else {
          return FALSE;
        }
    }
  }

  function successMessage($msg) {
    echo '<div class="row">
            <div class="col-lg-12"><div class="alert alert-success" style="border-radius:8px;">
            <button type="button" aria-hidden="true" class="close">×</button>
            <span>'.$msg.'</span>
          </div></div></div>';
  }

  function sendMail($to, $subject, $message) {
  # Instantiate the client.
    $mgClient = new Mailgun\Mailgun('');
    $domain = "osrs2go.com";

    # Make the call to the client.
    $result = $mgClient->sendMessage($domain, array(
      'from'    => 'system@osrs2go.com', 
      'to'      => $to, 
      'subject' => stripslashes($subject), 
      'text'    => $message
    ));
  }
}

?>
