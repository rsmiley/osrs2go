<?php

class server extends webcms {
	function ___construct() {
		if(!$this->dbactive()) {
			die('Server functionality is powerless here.');
		}
	}

	/*

	The purpose of this is so we can hook a log into each of the site processes as it will be critical to track the generation/charging of servers, purchase/usage of credits, API/Cron calls and runs.

	*/