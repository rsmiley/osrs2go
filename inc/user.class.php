<?php

class user extends webcms {
  
  function __construct() {
  }
  /*
  Table Structure
  Table: users
  - id
  - user
  - pass
  - rank
  */
  
  public function dbOn() {
    if(self::dbactive()) { return TRUE; } else { return FALSE; }
  }

  public function logout($user, $session) {
    if(!self::dbOn()) { return; } 
    unset($_SESSION['fb_access_token']);
    unset($_SESSION['userid']);
  }

  public function isLoggedIn() {
    if(!self::dbOn()) { return; } 
    if(isset($_SESSION['fb_access_token']) || isset($_SESSION['userid'])) {
      if(!isset($_SESSION['userid'])) {
        $d = self::getCurrentUser();
        $_SESSION['userid'] = $d['fbid'];
      }
      return true;
    } else {
      return false;
    }
  }

  public function isAdmin($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = "We can only check the rank of existing users."; }
    if(self::getRank($user) >= 500 || self::getRank($user) == '4') { return TRUE; } else { return FALSE; }
  }

  public function loginRestricted() {
    if(!self::dbOn()) { return; }
    if(!self::isLoggedIn()) {
      #if($_SERVER['REQUEST_URI'] == 'login-required/') {
      #define(lol, true);
      
      #}
    }
  }

  # Essential User public functions
  public function userExists($user) {
    if(!self::dbOn()) { return; } 
    $q = DB::queryFirstRow("SELECT fbid, id FROM users WHERE fbid=%i OR id=%i", $user, $user);
    if(is_null($q)) { return false; }  else { return TRUE; }
  }

  public function getCurrentUser() {
    if(!self::dbOn()) { return; } 
    if(!isset($_SESSION['fb_access_token']) && !isset($_SESSION['userid'])) { return $funfail = "We can't retrieve the data of the current user if there is none..."; }

    # Build query using session data.
    $u = DB::queryFirstRow("SELECT * FROM users WHERE access_token=%s OR fbid=%i", $_SESSION['fb_access_token'], $_SESSION['userid']);
    if(is_null($u)) {  return webcms::errorPage("We seem to have encountered an error registering you.<br /> Please first try clicking <a href='/force-logout'>this link</a> and then refreshing the page. <br />Try logging in one more time. If you see this error again, contact Roger Smiley."); }

    return $u;
  }

  public function getRank($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT rank FROM users where fbid = %i", $user);
    if(is_null($u)) { return $funfail = "No rank exists for this user / User not found"; }

    return $u['rank'];
  }

  public function getTextRank($user) {
    switch(self::getRank($user)) {
    case '501':
    $rank = '<b>Owner</b>';
    break;
    case '500':
    $rank = '<b>Co-Owner</b>';
    break;
    case '5':
    $rank = '<b>OSRS FB Senior Admin</b>';
    break;
    case '4':
    $rank = '<b>osrs2go Support Staff</b>';
    break;
    case '3':
    $rank = 'OSRS FB Staff';
    break;
    case '2':
    $rank = 'Beta User';
    break;
    case '1':
    $rank = 'User';
    break;
    default:
    $rank = 'Unknown';
    break;
  }
  return $rank;
  }

  public function getRealName($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT name FROM users where fbid = %i OR id = %i", $user, $user);
    if(is_null($u)) { return $funfail = "No name exists for this user / User not found"; }

    return $u['name'];
  }

  public function getCredits($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT credits FROM users where fbid=%i", $user);
    if(is_null($u)) { return $funfail = "No credit exists for this user / User not found"; }

    return $u['credits'];
  }

  public function hasReadGettingStarted($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT hasreadgettingstarted FROM users where fbid=%i", $user);
    if(is_null($u)) { return $funfail = "No credit exists for this user / User not found"; }

    if($u['hasreadgettingstarted'] == 0) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  public function getFBAuth($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT access_token FROM users where fbid=%i", $user);
    if(is_null($u)) { return $funfail = "No Token exists for this user / User not found"; }

    return $u['access_token'];
  }

  public function isBanned($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT banned FROM users where fbid=%i", $user);
    if(is_null($u)) { return $funfail = "No credit exists for this user / User not found"; }

    if($u['banned'] == 0) {
      return FALSE;
    } else {
      return TRUE;
    }
  }

  public function updateUser($user, $row, $val) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return $funfail = "You absolutely cannot update a user that doesn't exist."; }
    $columns = DB::columnList('users');
    $updates = array_combine($row, $val);
    foreach($updates as $r => $k) {
      if(!in_array($r, $columns)) { return $funfail = "You are trying to update an invalid column."; }
        DB::update("users", array(
      $r => $k), "fbid=%i OR id=%i", $user, $user);
    }
  }  

  public function getAllUsersAndID() {
    if(!self::dbOn()) { return; } 
    $q = DB::query("SELECT id,name FROM users");
    $names = array();
    foreach($q as $v) {
      array_push($names, $v['name'].' ('.$v['id'].')');
    }
    return json_encode($names);
  }

  public function getAllUsers() {
    if(!self::dbOn()) { return; } 
    $q = DB::query("SELECT * FROM users");
    return $q;
  }

  public function getUser($user) {
    if(!self::dbOn()) { return; } 
    if(!self::userExists($user)) { return 'Requested user does not exist.'; }
    $u = DB::queryFirstRow("SELECT * FROM users where id=%i", $user);
    if(is_null($u)) { return $funfail = "No Token exists for this user / User not found"; }

    return $u;
  }
}
?>