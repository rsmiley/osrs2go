<?php
	
class credits extends webcms {

	function __construct() {
	}

	function acceptPayment($json) {
		if($_SERVER['HTTP_USER_AGENT'] != 'PayPal IPN ( https://www.paypal.com/ipn )') { die(''); }
		// Use the sandbox endpoint during testing.
		if($_REQUEST['mc_currency'] != 'USD') { $_REQUEST['payment_status'] = 'Review'; }
		DB::insert('paypalTransLog', array(
        'fbid' => $_REQUEST['option_selection2'],
        'amount' => $_REQUEST['mc_gross'],
        'firstname' => $_REQUEST['first_name'],
        'lastname' => $_REQUEST['last_name'],
        'email' => $_REQUEST['payer_email'],
        'status' => $_REQUEST['payment_status'],
        'transactionid' => $_REQUEST['txn_id'],
        'processed' => 0,
        'time' => time(),
        'dump' => $json
      ));
	}

	function processPaymentQueue() {
		$l = DB::query("SELECT * FROM paypalTransLog WHERE processed = %i AND status = %s", 0, 'Completed');
		foreach($l as $k) {
		$value = round($k['amount'] * 2);
		if($k['amount'] == 2.88) { $value = 5; }
		# Purchase Bonuses #
		if($k['amount'] == 2.88) { $value = $value + 2; }
		if($k['amount'] == 5.00) { $value = $value + 5; }
		if($k['amount'] == 10.00) { $value = $value + 10; }
		user::updateUser($k['fbid'], array('credits'), array(user::getCredits($k['fbid'])+$value));
		DB::update('paypalTransLog', array('processed' => 1), "processed = %i", 0);
		DB::update('paypalTransLog', array('processedAmount' => $value), "id = %i", $k['id']);
		notify::newNotify($k['fbid'], "<b>Payment Accepted!</b><br />You've received {$value} credits");
		}
	}

}

?>