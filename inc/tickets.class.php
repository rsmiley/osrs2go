<?php

class tickets extends webcms {

	function __construct() {
		# Nothing bro.
	}

	function ticketExists($ticketid) {
		$d = DB::queryFirstRow("SELECT id FROM tickets WHERE id = %i", $ticketid);
		return print_r($d, true);
		if(is_null($d)) { return FALSE; } else { return TRUE; }
	}

	function showTicket($ticketid) {
		global $fb;
		if(!$this->ticketExists($ticketid)) { return webcms::errorPage("Invalid ticket request"); }
		$f = DB::queryFirstRow("SELECT * FROM tickets WHERE `id` = %i", $ticketid);
		# Admin Override?
		if(!user::isAdmin($_SESSION['userid'])) {
		if($f['ownerid'] != $_SESSION['userid']) { return webcms::errorPage("You are not authorized to view tickets that do not belong to you."); }
		}

		$response = $fb->get('/me?fields=first_name,last_name,picture', user::getFBAuth($f['ownerid']));
		  $ua = $response->getDecodedBody();
		  $userAvatar = $ua['picture']['data']['url'];
		  if($ua['picture']['data']['is_silhouette']) {
		    $userAvatar = 'http://i.imgur.com/x7asaZf.png'; # Default Bubble
		  }
  		$userName = $ua['first_name'].' '.$ua['last_name'];
  		$rank = user::getTextRank($f['ownerid']);
		$replytime = date('F j, Y', $f['time']).' at '.date('g:i a', $f['time']);
		print '<div class="card">
                                    <div class="header">
                                       <h4 class="title">Reply to Ticket - '.$f['subject'].'</h4>
                                    </div>
                                    <div class="content"> ';

        # Admin Controls
        if(user::isAdmin($_SESSION['userid'])) {
        if($f['status'] != 2) {
        	$clSw = "<input type='submit' class='btn btn-danger' name='closeTicket' value='Close Ticket' />";
        } else {
        	$clSw = "<input type='submit' class='btn btn-success' name='openTicket' value='Open Ticket' /> ";
        }
        $adPrint = "$clSw
        <input type='submit' class='btn btn-danger' name='pendingRoger' value='Mark Pending Roger' />
        <input type='submit' class='btn btn-danger' name='pendingReview' value='Mark Pending Review' />
        <input type='submit' class='btn btn-success' name='refundTicket' value='Refund 1 Credit' /> ";
    	}
        # Admin Controls end
		if($f['status'] != 2 || user::isAdmin($_SESSION['userid'])) {

                                    print '<form action="" method="POST">
                                      <textarea name="replymsg"></textarea><br />
                                      '.$adPrint.'<input type="submit" class="pull-right btn btn-success" name="submit" value="Submit Response" /></form><br /><br />';
        } else {
        	print '<center><h3>This ticket is closed for replies.</h3>If you need further help, please open a new ticket!</center>';
        }

                                      print '
                                    </div>    
                                  </div>    
                                </div> 
                                  
                                  <div class="col-lg-8">        
                                    <div class="card">
                                       <div class="header">
                                            <h4 class="title">Ticket Responses</h4>
                                    </div>
                                    <div class="content"> ';
		# We want to show the replies above the ticket.
		$this->getReplies($ticketid);
		print "<div class='row'>
                                        <div class='col-sm-2'>
                                          <div class='author' style='text-align:center; text-transform: none;'>
                                            <img class='avatar border-white' src='$userAvatar' alt='...'/>
                                            <h4 class='title'>$userName<br />
                                               <a href='#'><small><b>$rank</b></small></a>
                                               <div class='row''><div class='col-xsm-4'><small>$replytime</small></div></div>
                                            </h4>
                                          </div>
                                        </div>
                                        <div class='col-md-10'>
                                          {$f['message']}
                                        </div>
                                      </div>";
	}

	function replyToTicket($ticketid, $reply) {
		if(!$this->ticketExists($ticketid)) { return webcms::errorPage("Can't reply to a nonexistant ticket broski"); }
		if(empty($reply)) { return webcms::errorPage("There must be a response in that reply."); }
		if(!user::isAdmin($_SESSION['userid']) && !empty($_SESSION['userid'])) {
			webcms::sendMail("osrs2go@mailhub.co", "New reply to ticket submitted!", "Heyo!
A new ticket was submitted to osrs2go!

Reply: {$reply}

Here's a handy link to the ticket! 
https://osrs2go.com/show-ticket/?t={$ticketid}");
		}
		DB::insert('ticketReplies', array(
			'ticketid' => $ticketid,
			'fbid' => $_SESSION['userid'],
			'content' => $reply,
			'time' => time()
			));
		return TRUE;
	}

	function createTicket($subject, $content) {
		if(empty($subject) || empty($content)) { return webcms::errorPage("You must completely fill out the form in order to create a ticket."); }
		DB::insert('tickets', array(
			'subject' => $subject,
			'ownerid' => $_SESSION['userid'],
			'message' => $content,
			'time' => time(),
			'openedip' => $_SERVER['REMOTE_ADDR']
			));
		webcms::sendMail("osrs2go@mailhub.co", "New ticket ({$subject}) submitted!", "Heyo!
A new ticket was submitted to osrs2go!

Here's a handy link to the ticket! 
https://osrs2go.com/show-ticket/?t=".DB::insertId());
		return '/show-ticket/?t='.DB::insertId();
	}

	function getReplies($ticket) {
		global $fb;
		if(!$this->ticketExists($ticket)) { return webcms::errorPage("Invalid ticket request"); }
		$c = DB::query("SELECT * FROM ticketReplies WHERE `ticketid` = %i ORDER BY id DESC", $ticket);
		foreach($c as $f) {
		$response = $fb->get('/me?fields=first_name,last_name,picture', user::getFBAuth($f['fbid']));
		  $ua = $response->getDecodedBody();
		  $userAvatar = $ua['picture']['data']['url'];
		  if($ua['picture']['data']['is_silhouette']) {
		    $userAvatar = 'http://i.imgur.com/x7asaZf.png'; # Default Bubble
		  }
  		$userName = $ua['first_name'].' '.$ua['last_name'];
  		$rank = user::getTextRank($f['fbid']);
		$replytime = date('F j, Y', $f['time']).' at '.date('g:i a', $f['time']);
		print "<div class='row'>
                                        <div class='col-sm-2'>
                                          <div class='author' style='text-align:center; text-transform: none;'>
                                            <img class='avatar border-white' src='$userAvatar' alt='...'/>
                                            <h4 class='title'>$userName<br />
                                               <a href='#'><small><b>$rank</b></small></a>
                                               <div class='row''><div class='col-xsm-4'><small>$replytime</small></div></div>
                                            </h4>
                                          </div>
                                        </div>
                                        <div class='col-md-10'>
                                          {$f['content']}
                                        </div>
                                      </div><hr />";
                                  }
	}

	function userTicketList($user) {
		if(!user::isLoggedIn()) { return webcms::errorPage("You must be logged in to see your tickets."); }
		$l = DB::query("SELECT * FROM tickets WHERE ownerid = %i ORDER BY id DESC", $user);
		return $l;
	}

	function adminTicketList() {
		$l = DB::query("SELECT * FROM tickets ORDER BY id DESC");
		return $l;
	}

	function status2text($statuscode) {
		switch($statuscode) {
			case '1':
			$txt = 'Open';
			break;
			case '2':
			$txt = 'Closed';
			break;
			case '3':
			$txt = 'Pending Roger';
			break;
			case '4':
			$txt = 'Pending Review';
			break;
			case '5':
			$txt = 'Closed & Saved';
			break;
			case '6':
			$txt = 'Pending Payment';
			break;
			default:
			$txt = 'Unknown';
			break;
		}
		return $txt;
	}

	function updateStatus($ticket, $status) {
		if(!self::ticketExists($ticket)) { return webcms::errorPage("We can't update the status of a ticket that we can't find!"); }
		DB::update('tickets', array(
			'status' => intval($status)
			), "id = %i", $ticket);
	}

	function refundCredit($ticketid) {
		if(!self::ticketExists($ticketid)) { return webcms::errorPage("We can't refund a ticket that we can't find!"); }
		$f = DB::queryFirstRow("SELECT * FROM tickets WHERE `id` = %i", $ticketid);
		$user = $f['ownerid'];
		self::replyToTicket($ticketid, "<i><b>System Response:</i> 1 credit was refunded</b>");
		user::updateUser($user, array('credits'), array(user::getCredits($user)+1));
		DB::insert('transactionLog', array(
			'serverid' => 'TICKETID: '.$ticketid,
			'amount' => 1,
			'user' => $user,
			'time' => time()
			));
	}
}


?>