<?php
# home.php
# [Template File] | Index Page | Written by Roger Smiley
$deets = $cms->getPage(key($_GET));

if(!$user->isLoggedIn() && $deets['0']['loggedinonly'] == 1) {
  $deets = $cms->getPage('login-required');
}
if($deets['0']['crumb'] == 'getting-started' && $user->isLoggedIn()) {
  if(!$user->hasReadGettingStarted($_SESSION['userid'])) {
    $user->updateUser($_SESSION['userid'], array('hasreadgettingstarted'), array('1'));
  }
}
if($deets['0']['adminonly'] == 1 && !$user->isAdmin($_SESSION['userid'])) {
  $deets['0']['title'] = 'Access Denied';
  $deets['0']['content'] = "You're not authorized to view this page at this time.";
}

$deets['0']['content'] = str_replace('%SESSION_ID%', $_SESSION['userid'], $deets['0']['content']);
?>

<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                                 <div class="col-lg-8">        <div class="card">

                                              <div class="header">

                                            <h4 class="title"><?php echo $deets['0']['title']; ?></h4>
                                    </div>
                                    <div class="content">        
                                    <?php echo $deets['0']['content']; ?>
                                  </div>    </div>    
                                   <?php # end of column ?>  
                                  </div> <?php # end of column ?>
                                  </div> <?php # end of column ?>
                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> <?php # end of column ?>
                                    */?>