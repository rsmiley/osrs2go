<?php
# home.php
# [Template File] | Index Page | Written by Roger Smiley
if(!$user->isLoggedIn()) {
  $cms->errorPage("You must be logged in to view tickets.");
}

$v = str_replace('/show-ticket/?t=', '', $_SERVER['REQUEST_URI']);
?>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=kbm23af0l76n1fyu7wb19riju81q2ohmma50gwjid4ya1azj"></script>
  <script>tinymce.init({
  selector: 'textarea'});
  </script>
<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                               <div class="col-lg-8">   
                               <?php
                               if(isset($_POST['submit'])) {
                                  if($tickets->replyToTicket($v, $_POST['replymsg'])) {
                                    $cms->successMessage("Your ticket response was successfully recorded.<br />We'll get back to you as soon as possible!");
                                    }
                                  } 

                                  # Admin Functions
                                  if($user->isAdmin($_SESSION['userid'])) {
                                    if(isset($_POST['closeTicket'])) {
                                      $tickets->updateStatus($v, 2);
                                      $cms->successMessage("The ticket has been closed.");
                                    }
                                    if(isset($_POST['openTicket'])) {
                                      $tickets->updateStatus($v, 1);
                                      $cms->successMessage("The ticket has been reopened.");
                                    }
                                    if(isset($_POST['pendingRoger'])) {
                                      $tickets->updateStatus($v, 3);
                                      $cms->successMessage("The ticket has been marked Pending Roger.");
                                    }
                                    if(isset($_POST['pendingReview'])) {
                                      $tickets->updateStatus($v, 4);
                                      $cms->successMessage("The ticket has been marked for further review.");
                                    }
                                    if(isset($_POST['refundTicket'])) {
                                      $tickets->refundCredit($v);
                                      $cms->successMessage("The user has been given 1 credit.");
                                    }
                                  }
                                ?>
                                         
                                      
                                      <?php 
                                      #$tickets->getReplies($v);
                                      $tickets->showTicket($v); ?>
                                      

                                    </div>    
                                    </div>    
                                  </div>    
    </div>    
    </div>    
                                     

                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> 
                                    */?>