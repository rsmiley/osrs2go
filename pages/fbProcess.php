<?php
if(!session_id()) {
    session_start();
}
if(file_exists('../inc/db.php')) {
        include "../inc/db.php";
        include "../inc/dbClass.php";
        new DB();
        DB::$user = $dbCon['username'];
        DB::$password = $dbCon['password'];
        DB::$dbName = $dbCon['dbname'];
      }
date_default_timezone_set('America/Phoenix');
require_once("../vendor/autoload.php");
//Call Facebook API
$fb = new Facebook\Facebook([
  'app_id' => '', // Replace {app-id} with your app id
  'app_secret' => '',
  'default_graph_version' => 'v2.5',
  ]);


//Include Facebook SDK
$appId = ''; //Facebook App ID
$appSecret = ''; // Facebook App Secret
$redirectURL = ''; // Callback URL
$fbPermissions = 'email';  //Required facebook permissions
$helper = $fb->getRedirectLoginHelper();

try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (! isset($accessToken)) {
  if ($helper->getError()) {
    header('HTTP/1.0 401 Unauthorized');
    echo "Error: " . $helper->getError() . "\n";
    echo "Error Code: " . $helper->getErrorCode() . "\n";
    echo "Error Reason: " . $helper->getErrorReason() . "\n";
    echo "Error Description: " . $helper->getErrorDescription() . "\n";
  } else {
    header('HTTP/1.0 400 Bad Request');
    echo 'Bad request';
  }
  exit;
}

$oAuth2Client = $fb->getOAuth2Client();
// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId($appId); // Replace {app-id} with your app id
$tokenMetadata->validateExpiration();

if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
  try {
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
    exit;
  }

  echo '<h3>Long-lived</h3>';
}
  

  $response = $fb->get('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', $accessToken);
  $r_data = $response->getDecodedBody();
  $_SESSION['fb_access_token'] = (string) $accessToken;

  $q = DB::queryFirstRow("SELECT fbid FROM users WHERE fbid=%i", $r_data['id']);
  if(is_null($q)) {
    # Free Credits on Registration
      $freecredits = 0;
    if($r_data['picture']['data']['is_silhouette'] == TRUE) {
      $freecredits = 0;
      # These users get no credits as part of an abuse deterent.
    }
    if(empty($r_data['email'])) {
      $r_data['email'] = '*none provided*';

    }
DB::insert('fb_datadump', array(
    'fbid' => $r_data['id'],
    'datadump' => $r_data['first_name'].' '.$r_data['last_name'],
    'access_token' => (string) $accessToken,
    'profiledump' => json_encode($r_data),
    'email' => $r_data['email']
  ));
sleep(2);
DB::insert('users', array(
    'name' => $r_data['first_name'].' '.$r_data['last_name'],
    'email' => $r_data['email'],
    'access_token' => (string) $accessToken,
    'fbid' => $r_data['id'],
    'dumpdataID' => DB::insertId(),
    'credits' => $freecredits,
    'lastknownip' => $_SERVER['REMOTE_ADDR']
  ));
} else {
  DB::update('users', array(
    'access_token' => (string) $accessToken,
    'lastknownip' => $_SERVER['REMOTE_ADDR']
    ), "fbid=%i", $r_data['id']);
}
sleep(1);
header('Location: /welcome');

?>
