<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                                     <div class="col-lg-8">        

                                     <?php 
                                     $cms->successMessage("Beta Testing mode is active! All new users get 1 credit upon login.<br /><b>Please read the <a href='/getting-started/'>Getting Started</a> guide before requesting a server!</b>");
                                     ?>
                                     
                                     <div class="card">

                                              <div class="header">

                                            <h4 class="title">Welcome to OSRS2go</h4>
                                    </div>
                                    <div class="content">        
                                    <p>We aim to provide a new way to play Oldschool Runescape by utilizing a Remote Desktop over your phone.
                                  We have spent months trying out many different methods to stream a remote desktop without any FPS lag.
                                  It has finally been accomplished, and we have managed to "perfect" the method.
                                  Using our servers, you can play lag free (internet connection allowing) on your phone (iOS / Android), tablet, chromebook, PC, and any other device with a remote desktop application.</p>
                                  <p>We offer multiple classes of servers to keep your mobile costs low. We charge you <b>per launch</b> when you activate a server. Every level of server provided will have a <b>3 hour lifetime</b> before they shut off automatically.
                                  Once your time has expired, you will need to make a new server and continue your progress from there. However; don't worry! All our servers are ready to go right out of the box. You connect with a temporary password and set your own private one on first connect! </p>
                                  </div>    </div>    <div class="span8">
                                  <div class="card">
                                  <div class="header">

                                          <h4 class="title">Server Level Tiers</h4>
                                  </div>
                                  <div class="content">      
                                  <p><div class="db-wrapper">
                                  <div class="db-pricing-nine">
                                    <div class="table-responsive">
                                    <table class="table table-hover">

                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="db-bk-color-one">Level 1</th>
                                            <th class="db-bk-color-two">Level 2</th>
                                            <th class="db-bk-color-three">Level 3</th>
                                            <!-- <th class="db-bk-color-three">Always On</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                          <td class="db-width-perticular">Average FPS</td>
                                          <td>20 - 40</td>
                                          <td>Steady 30+</td>
                                          <td>Steady 40+</td>
                                         <!--  <td>TBD</td> -->
                                        </tr>
                                        <tr>
                                            <td class="db-width-perticular">RAM</td>
                                            <td>3.75gb</td>
                                            <td>7.5gb</td>
                                            <td>15gb</td>
                                          <!--  <td>TBD</td> -->
                                          </tr>
                                          <tr>
                                              <td class="db-width-perticular">System Cores</td>
                                              <td>1</td>
                                              <td>2</td>
                                              <td>4</td>
                                           <!--    <td>TBD</td> -->
                                            </tr>
                                            <tr>
                                                <td class="db-width-perticular">Max Open Clients</td>
                                                <td>2</td>
                                                <td>2 - 5</td>
                                                <td>2 - 10</td>
                                             <!--   <td>TBD</td> -->
                                              </tr>
                                              <tr>
                                                  <td class="db-width-perticular">Preinstalled Clients</td>
                                                  <td>OSBuddy <br /> Konduit <br /> Official Client</td>
                                                  <td>OSBuddy <br /> Konduit <br /> Official Client</td>
                                                  <td>OSBuddy <br /> Konduit <br /> Official Client</td>
                                              <!--    <td>OSBuddy <br /> Konduit <br /> Official Client</td> -->
                                                </tr>
                                            <tr>
                                                <td class="db-width-perticular">Cost Per Setup</td>
                                                <td>1 Credit</td>
                                                <td>2 Credits</td>
                                                <td>3 Credits</td>
                                              <!--  <td>TBD</td> -->
                                              </tr><!--
                                        <tr>
                                            <td class="db-width-perticular"></td>
                                            <td><a href="#" class="btn btn-success">Start Server</a> </td>
                                            <td><a href="#" class="btn btn-success">Start Server</a> </td>
                                            <td><a href="#" class="btn btn-success">Start Server</a> </td>
                                            <td><a href="#" class="btn btn-success">Request Server</a> </td>
                                            </tr>-->
                                    </tbody>
                                    </table>
                                    </div>
                                  </div>
                                  </div>         
                                     </div></p>
                                  </div>
                                  </div>
                                  </div> <?php # end of column ?>  
                                  </div> <?php # end of column ?>
                                  </div> <?php # end of row ?>
                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> <?php # end of column ?>
                                    */?>