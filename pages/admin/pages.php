<?php
# home.php
# [Template File] | Index Page | Written by Roger Smiley
?>

<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                                 <div class="col-lg-8">        <div class="card">

                                              
                                    <div class="content">        
                                      <table class="table table-hover">

                                        <thead>
                                        <tr>
                                            <th class="db-bk-color-one">Page Title</th>
                                            <th class="db-bk-color-two">Access Crumb</th>
                                            <th class="db-bk-color-three">Last Edited</th>
                                            <th class="db-bk-color-three">Flags</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $l = DB::query("SELECT * FROM pages");
                                    foreach($l as $a) {
                                      if($a['lastedit'] == 0) {
                                        $le = 'Never';
                                      } else {
                                      $le = date('n/j/y @ h:i A', $a['lastedit']);
                                      }
                                      if($a['loggedinonly'] == 1) {
                                        $flag = " <i title='Must be Logged In' class='ti-user' style='font-size:18px;'></i>";
                                      }
                                      if($a['adminonly'] == 1) {
                                        $flag .= " <i title='Admin Only' class='ti-lock' style='color:red; font-size:18px;'></i>";
                                      }
                                      print "<tr>
                                          <td><a href='/edit-page/?p={$a['id']}'><b>{$a['title']}</b></a></td>
                                          <td><a target='_blank' href='/{$a['crumb']}'>{$a['crumb']}</a></td>
                                          <td>{$le}</td>
                                          <td>{$flag}</td>
                                        </tr>";
                                        $flag = "";
                                    }
                                    ?>
                                      
                                    </tbody>
                                    </table>
                                    <a class='pull-right btn btn-success' href='/new-page'>New Page</a>
                                   <br /><br /> 
                                  </div>    </div>    
                                   <?php # end of column ?>  
                                  </div> <?php # end of column ?>
                                  </div> <?php # end of column ?>
                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> <?php # end of column ?>
                                    */?>