<?php
$v = str_replace('/userAdmin/?userSearch=', '', $_SERVER['REQUEST_URI']);
preg_match("/\((.*?)\)/", urldecode($v), $userSe);
$thisUser = $user->getUser($userSe['1']);
?>
<style>
.form-horizontal .control-label {
	text-align: left !important;
	padding-left: 20px;
	width: 30% !important;
}
.form-horizontal .controls {
	margin-left: 70px !important;
	width: 100% !important;
}
.fix_hgt {
	height: 384px !important;
}
</style>
<div id="content">


  <div id="content-header">

    <div id="breadcrumb"> <a href="/admin/" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home > User Manager</a> </div>

  </div>
  <div class="container-fluid">
<?php include("statusboxes.php");
if($thisUser == 'Requested user does not exist.') {
	echo "<center><h1>There is no user with the requested information</h1></center>";
	include('dashboard.php');
	include('/home/osrs2go/public_html/style/osrs2go-admin/footer.php');
	exit();
}
?>
                  <div class="row-fluid">

<div class="span3">
<?php
if(isset($_POST['userUpdateSubmit'])) {
  if($user->getRank($_SESSION['userid']) < 499) {
    print "We do not allow non-owners to modify users at this time.";
    exit();
  }
	# Process user updates
	unset($_POST['userUpdateSubmit']); # Because you didn't know why we did this at first glance.
  # ^ This is done because we read the values from the $_POST and this doesn't match the database row.
	print '<div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
<h4 class="alert-heading">Success!</h4><p>';

	foreach($_POST as $k => $v) {
		if($v != $thisUser[$k]) {
			$user->updateUser($thisUser['fbid'], array($k), array($v));
			echo "Updated <b>{$k}</b> to {$v}<br />";
		} else {
			#echo "Skipped update for {$k} - It's equal <br />";
		}
	}
	print "</p></div>";
}
?>
<div class="widget-box">
<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
<h5>User Editor</h5>
</div>
<div class="widget-content nopadding">
<form class="form-horizontal" action='' method='POST'>
<fieldset>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="fullname">Full Name</label>
  <div class="controls">
    <input id="fullname" name="name" type="text" class="input-large" value="<?php echo $thisUser['name']; ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="email">Email</label>
  <div class="controls">
    <input id="email" name="email" type="text" class="input-large" value="<?php echo $thisUser['email']; ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="fbid">Facebook ID</label>
  <div class="controls">
    <input id="fbid" name="fbid" type="text" class="input-large" disabled value="<?php echo $thisUser['fbid']; ?>">
    <p class="help-block" align='center'><a href='https://fb.com/<?php echo $thisUser['fbid']; ?>' target="_blank">Link to Profile</a></p>
  </div>
</div>

<!-- Text input-->
<div class="control-group">
  <label class="control-label" for="credits">Credits</label>
  <div class="controls">
    <input id="credits" name="credits" type="text" class="input-large" value="<?php echo $thisUser['credits']; ?>">
    
  </div>
</div>

<div class="control-group">
  <label class="control-label" for="selectbasic">User Rank</label>
  <div class="controls">
  <?php 
  if($thisUser['rank'] > 5) { 
  	$echo = 'disabled'; 
  	} 
  	$currentRank = $user->getTextRank($thisUser['fbid']);
  	$validrank = "<option value='{$thisUser['rank']}'>{$currentRank}</option>";
  	?>
    <select id="rank" name="rank" class="input-large" <?php echo $echo; ?>>
    <?php echo $validrank; ?>
      <option value='1'>User</option>
      <option value='2'>Beta Tester</option>
      <option value='3'>OSRS FB Staff</option>
      <option value='5'>OSRS FB Senior Admin</option>
      <option value='4'>osrs2go Support Staff</option>
    </select>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="control-group">
  <label class="control-label" for="gotstarted">Got Started?</label>
  <div class="controls">
    <label class="radio inline" for="gotstarted-0">
      <input type="radio" name="hasreadgettingstarted" id="gotstarted-0" value="1" checked="checked">
      Yes
    </label>
    <label class="radio inline" for="gotstarted-1">
      <input type="radio" name="hasreadgettingstarted" id="gotstarted-1" value="0">No</label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="control-group">
  <label class="control-label" for="banned">Banned</label>
  <div class="controls">
    <label class="radio inline" for="banned-0">
      <input type="radio" name="banned" id="banned-0" value="1">
      Yes
    </label>
    <label class="radio inline" for="banned-1">
      <input type="radio" name="banned" id="banned-1" value="0"  checked="checked">
      No
    </label>
  </div>
</div>

<!-- Button -->
<div class="control-group">
  <label class="control-label" for="userUpdateSubmit"></label>
  <div class="controls">
    <button id="userUpdateSubmit" name="userUpdateSubmit" class="btn btn-success">Submit</button>
  </div>
</div>

</fieldset>
</form>



</div>
</div>
</div>


          
          <div class="span5">
            <div class="widget-box ">
              <div class="widget-title">
                <?php
				$getServerList = $servers->getAllUsersServers($thisUser['fbid']);
				$jd = json_decode($getServerList, true);
                $badgeCount = count($jd);

				$getPurchases = DB::query("SELECT * FROM paypalTransLog WHERE fbid = %i ORDER BY id DESC", $thisUser['fbid']);
                $purchaseCount = count($getPurchases);

                $getSupport = $tickets->userTicketList($thisUser['fbid']);
                $supportCount = count($getSupport);
                ?>
            <ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab1">Server History <span class="badge badge-info"><?php echo $badgeCount; ?></span></a></li>
				<li><a data-toggle="tab" href="#tab2">Purchase History <span class="badge badge-success"><?php echo $purchaseCount; ?></span></a></li>
				<li><a data-toggle="tab" href="#tab3">Support History <span class="badge badge-inverse"><?php echo $supportCount; ?></span></a></li>
				</ul>
				</div>
				<div class="widget-content tab-content fix_hgt">
				<div id="tab1" class="tab-pane active">

				<?php
				foreach($jd as $k) {
					$serverdate = date('n/j/y @ h:i A', $k['started']);
					$day = date('j', $k['started']);
					$mth = date('M', $k['started']);
					switch($k['pinged']) {
						case '1':
						$pinged = 'Yes';
						break;
						case '0':
						$pinged = 'No';
						break;
					}
					echo "<div class='new-update clearfix'> 
							<span class='update-notice'> 
							<a href='#' title=''><strong>{$k['servername']} </strong></a> 
							<span><small><b>Started @</b> {$serverdate} | <b>Level:</b> {$k['level']} | <b>QuickDNS:</b> {$k['quickdns']} | <b>Used:</b> {$pinged}</small></span> 
							</span> 
							<span class='update-date'><span class='update-day'>{$day}</span>{$mth}</span> 
							</div>
					";
				}
				?>

				

				</div>
				<div id="tab2" class="tab-pane">
				<?php
				foreach($getPurchases as $k) {
					$day = date('j', $k['time']);
					$mth = date('M', $k['time']);
					switch($k['processed']) {
						case '1':
						$processed = 'Yes';
						break;
						case '0':
						$processed = 'No';
						break;
					}
					$money = '$'.number_format($k['amount'], 2);
					echo "<div class='new-update clearfix'> 
							<span class='update-notice'> 
							<a href='#' title=''><strong>{$k['firstname']} {$k['lastname']} for {$k['processedAmount']} credits ({$money})</strong></a> 
							<span><small><b>Email:</b> {$k['email']} | <b>Transaction ID:</b> {$k['transactionid']} <br />
							<b>Status:</b> {$k['status']} | <b>Processed:</b> {$processed}
							</small></span> 
							</span> 
							<span class='update-date'><span class='update-day'>{$day}</span>{$mth}</span> 
							</div>
					";
				}
				?>
				</div>
				<div id="tab3" class="tab-pane">
				<?php
				foreach($getSupport as $k) {
					$serverdate = date('n/j/y @ h:i A', $k['time']);
					$day = date('j', $k['time']);
					$mth = date('M', $k['time']);
					$ticketStatus = $tickets->status2text($k['status']);
					echo "<div class='new-update clearfix'> 
							<span class='update-notice'> 
							<a href='https://osrs2go.com/show-ticket/?t={$k['id']}' target='_blank' title=''><strong>{$k['subject']} </strong></a> 
							<p>{$k['message']}</p>
							<span><small><b>Opened @</b> {$serverdate} | <b>Status:</b> {$ticketStatus} | <b>OpenIP:</b> {$k['openedip']}</small></span> 
							</span> 
							<span class='update-date'><span class='update-day'>{$day}</span>{$mth}</span> 
							</div>
					";
				}
				?>
				</div>
				</div>
          </div>
        </div>

        <div class="span4">
            <div class="widget-box">
              <div class="widget-title">
                <span class="icon">
                  <i class="icon-arrow-right"></i>
                </span>
                <h5>User Error Log</h5>
              </div>
              <div class="widget-content nopadding fix_hgt">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Occured At</th>
                      <th>Error</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $dev = DB::query("SELECT * FROM `serverErrorLog` WHERE `user` = %s ORDER BY id DESC LIMIT 30", $thisUser['name']);
                    foreach($dev as $de) {
                    $date = date('F j, Y', $de['time']).' at '.date('g:i a', $de['time']);
                      echo "<tr>
                              <td><center>{$de['user']}</center></td>
                              <td><center>{$date}</center></td>
                              <td><center>{$de['error']}</center></td>
                            </tr>
                              ";
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
</div>

<hr>
<?php include("dashboard.php"); ?>


</div>


</div>
