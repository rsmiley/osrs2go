  <?php
$userC = count($user->getAllUsers());
$serverC = count(json_decode($servers->getAllServers()));
$weekTime = strtotime('-1 week');
$now = time();
$serverWC = DB::query("SELECT * FROM servers WHERE started BETWEEN {$weekTime} AND $now");
$sWC = count($serverWC);
$ocSQL = DB::query("SELECT * FROM paypalTransLog");
$orderCount = count($ocSQL);
$openCountsql = DB::query("SELECT * FROM tickets WHERE status = 1");
$openCount = count($openCountsql);
$closedCountsql = DB::query("SELECT * FROM tickets WHERE status != 1");
$closedCount = count($closedCountsql);
  $getTotalValue = DB::query("SELECT * FROM paypalTransLog");
  $displayMoney = 0;
  $transCount = 0;
  foreach($getTotalValue as $dollar) {
    $displayMoney = $displayMoney + $dollar['amount'];
    $transCount++;
  }
  $displayMoney = number_format($displayMoney, 2);
?>

  <center>
    <div class="widget-box widget-plain">
      <div class="center">
        <ul class="stat-boxes">
          <li>
          <div class="left"><b class='fa fa-address-card-o' style='font-size:3.5em;'></b></div>
            <div class="right"> <strong><?php echo $userC; ?></strong> Total Users </div>
          </li>
          <li>
          <div class="left"><b class='fa fa-server' style='font-size:3.5em;'></b></div>
            <div class="right"> <strong><?php echo $serverC; ?></strong> Total Servers</div>
          </li>
          <li>
          <div class="left"><b class='fa fa-tasks' style='font-size:3.5em;'></b></div>
            <div class="right"> <strong><?php echo $sWC; ?></strong> Servers this Week</div>
          </li>
          <li>
          <div class="left"><b class='fa fa-shopping-basket' style='font-size:3.5em;'></b></div>
            <div class="right"> <strong><?php echo $orderCount; ?></strong> Total Purchases</div>
          </li>
          <li>
          <div class="left"><b class='fa fa-dollar' style='font-size:3.5em;'></b></div>
            <div class="right"> <strong style='color: green;'>$<?php echo $displayMoney; ?></strong> Total Earnings</div>
          </li>
          <li>
          <div class="left"><b class='fa fa-meh-o' style='font-size:3.5em;'></b></div>
            
            <div class="right"> <strong><?php echo $openCount.' / '.$closedCount; ?></strong> Open / Closed Tickets</div>
          </li>
        </ul>
      </div>
    </div>
    </center>
