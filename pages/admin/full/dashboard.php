
                  <div class="row-fluid">
          <div class="span4">
            <div class="widget-box">
              <div class="widget-title">
                <span class="icon">
                  <i class="icon-eye-open"></i>
                </span>
                <h5>Recent Server Launches</h5>
              </div>
              <div class="widget-content nopadding fix_hgt">
                <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Server Name</th>
                                            <th>Owner</th>
                                            <th>Level</th>
                                            <th>QuickDNS</th>
                                            <th>Running</th>
                                        </tr>
                                    </thead>
                                    <tbody>    
                                    <?php 
                                    $d = DB::query("SELECT * FROM servers ORDER BY id DESC LIMIT 15");
                                    foreach($d as $k) {
                                      $realName = $user->getRealName($k['owner']);
                                      if($k['running'] == 0) {
                                        $running = 'No';
                                      } else {
                                        $running = '<b>Yes</b>';
                                      }
                                      if($k['pinged'] == 1) {
                                        $pingyay = "class='success'";
                                      } else {
                                        $pingyay = "class='danger'";
                                      }
                                    $print .= "<tr $pingyay>
                                      <td><center>{$k['servername']}</center></td>
                                      <td><center>{$realName}</center></center></td>
                                      <td><center>{$k['level']}</center></td>
                                      <td><center>{$k['quickdns']}</center></td>
                                      <td><center>{$running}</center></td>
                                      </tr>";
                                    }
                                    echo $print;
                                    ?>
                                      </tbody>
                                    </table>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="widget-box">
              <div class="widget-title">
                <span class="icon">
                  <i class="icon-arrow-right"></i>
                </span>
                <h5>Last 15 Registrations</h5>
              </div>
              <div class="widget-content nopadding fix_hgt">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Full Name</th>
                      <th>Credits</th>
                      <th>Got Started</th>
                      <th>Launched Server</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $qry = DB::query("SELECT * FROM users ORDER BY id DESC LIMIT 15");

                    foreach($qry as $yo) {
                      if(empty($yo['lastinstanceid']) && $yo['laststart'] == 0) {
                        $started = 'No';
                      } else {
                        $started = '<small><b style="color:green;">Yes</b></small>';
                      }

                      if($yo['hasreadgettingstarted'] == 0) {
                        $read = 'No';
                      } else {
                        $read = '<small><b style="color:green;">Yes</b></small>';
                      }

                      echo "<tr>
                              <td><center>{$yo['name']}</center></td>
                              <td><center>{$yo['credits']}</center></td>
                              <td><center>$read</center></td>
                              <td><center>$started</center></td>
                            </tr>
                              ";
                    }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="span4">
            <div class="widget-box">
              <div class="widget-title">
                <span class="icon">
                  <i class="icon-file"></i>
                </span>
                <h5>Recent System Errors</h5>
              </div>
              <div class="widget-content nopadding fix_hgt">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Occured At</th>
                      <th>Error</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $dev = DB::query("SELECT * FROM `serverErrorLog` ORDER BY id DESC LIMIT 15");
                    foreach($dev as $de) {
                    $date = date('F j, Y', $de['time']).' at '.date('g:i a', $de['time']);
                      echo "<tr>
                              <td><center>{$de['user']}</center></td>
                              <td><center>{$date}</center></td>
                              <td><center>{$de['error']}</center></td>
                            </tr>
                              ";
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
