<?php
# home.php
# [Template File] | Index Page | Written by Roger Smiley

# This is a ghetto fix. It's not good, but it'll fuckin' do.
$v = str_replace('/edit-page/?p=', '', $_SERVER['REQUEST_URI']);

if(isset($_POST['submit'])) {
  # We update the post here. Don't do anything dumb Roger.
    if(!isset($_POST['loggedinonly'])) {
    $_POST['loggedinonly'] = 0;
    }
    if(!isset($_POST['adminonly'])) {
      $_POST['adminonly'] = 0;
    }

  DB::update('pages', array(
    'title' => $_POST['posttitle'],
    'content' => $_POST['postcontent'],
    'loggedinonly' => $_POST['loggedinonly'],
    'adminonly' => $_POST['adminonly'],
    'crumb' => $_POST['postcrumb'],
    'lastedit' => time(),
    ), "id=%i", $v);
  $toecho = '<div class="alert alert-success">
   <button type="button" aria-hidden="true" class="close">×</button>
    <span><b> Page Successfully Updated</b></span>
  </div>';
}
$deets = $cms->getPage(intval($v));
if($deets['0']['loggedinonly'] == 1) {
  $damn = 'checked';
}
if($deets['0']['adminonly'] == 1) {
  $damns = 'checked';
}

?>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=kbm23af0l76n1fyu7wb19riju81q2ohmma50gwjid4ya1azj"></script>
  <script>tinymce.init({
  selector: 'textarea',
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
  ],
  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
  image_advtab: true
 });</script>
<style>
  .icons {
    display: none;
  }
  </style>

<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                                 <div class="col-lg-8">        <div class="card">
                                              <div class="header">
                                      <form action='' method='POST'>
                                            <h4 class="title">
                                 <?php echo $toecho; ?>
                                            <b>Page Title:</b> <input type='text' name='posttitle' value='<?php echo $deets['0']['title']; ?>' />

                                            <span class='pull-right'><b>Page Crumb:</b> <input type='text' name='postcrumb' value='<?php echo $deets['0']['crumb']; ?>' /></span>
                                            </h4>
                                    </div>
                                    <div class="content">        
                                    <textarea name='postcontent'><?php echo $deets['0']['content']; ?></textarea> <br />
                                    Restrict to 
                                    ( <input type='checkbox' name='loggedinonly' <?php echo $damn; ?> value='1' /> Logged In Users) 
                                    ( <input type='checkbox' name='adminonly' <?php echo $damns; ?>  value='1' /> Admin) only?
                                    <input type='submit' class='pull-right btn btn-success' name='submit' value='Save Changes' /></form><br /><br />
                                  </div>    </div>    
                                   <?php # end of column ?>  
                                  </div> <?php # end of column ?>
                                  </div> <?php # end of column ?>
                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> <?php # end of column ?>
                                    */?>