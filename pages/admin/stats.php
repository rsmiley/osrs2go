<?php
# home.php
# [Template File] | Index Page | Written by Roger Smiley
$allSvr = json_decode($servers->getAllServers(), true);
$allRunSvr = json_decode($servers->getRunning(), true);
$u = $user->getAllUsers();
$cctvountAll = count($allSvr);
$cctvountRunning = count($allRunSvr);
$cctvountu = count($u);
$cctv = 0;
foreach($u as $k) {
  if($k['rank'] <= 4) {
      $cctv = $cctv + $k['credits'];
    }
}
$iu = DB::query("SELECT * FROM fb_datadump");
$iuc = count($iu);
  # Count Leveled Servers #
  $lvl1 = 0;
  $lvl2 = 0;
  $lvl3 = 0;
  foreach($allSvr as $k) {
    if($k['level'] == 1) { $lvl1++; }
    if($k['level'] == 2) { $lvl2++; }
    if($k['level'] == 3) { $lvl3++; }
  }
  $pj1 = $lvl1 * 0.5;
  $pj2 = $lvl2 * 1;
  $pj3 = $lvl3 * 1.5;
  $totalpj = $pj1 + $pj2 + $pj3;
?>
 <div class="row">

                </div>

<div class="row">
  <div class="col-lg-4 col-md-5">
                <?php include "pages/userBlock.php"; ?>
                    <div class="col-lg-8">

                                 <div class="card">

                                              
                                    <div class="content" style='height: 600px; overflow: scroll; overflow-x: hidden;'>

                                      <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="db-bk-color-one">Server Name</th>
                                            <th class="db-bk-color-two">Owner</th>
                                            <th class="db-bk-color-three">Zone</th>
                                            <th class="db-bk-color-four">Level</th>
                                            <th class="db-bk-color-five">IP Address</th>
                                            <th class="db-bk-color-six">QuickDNS</th>
                                            <th class="db-bk-color-seven">Running</th>
                                        </tr>
                                    </thead>
                                    <tbody>    
                                    <?php 
                                    $d = DB::query("SELECT * FROM servers ORDER BY id DESC");
                                    foreach($d as $k) {
                                      $realName = $user->getRealName($k['owner']);
                                      if($k['running'] == 0) {
                                        $running = 'No';
                                      } else {
                                        $running = '<b>Yes</b>';
                                      }
                                      if($k['pinged'] == 1) {
                                        $pingyay = "class='success'";
                                      } else {
                                        $pingyay = "class='danger'";
                                      }
                                    $print .= "<tr $pingyay>
                                      <td>{$k['servername']}</td>
                                      <td>{$realName}</td>
                                      <td>{$k['zone']}</td>
                                      <td>{$k['level']}</td>
                                      <td>{$k['serverip']}</td>
                                      <td>{$k['quickdns']}</td>
                                      <td>{$running}</td>
                                      </tr>";
                                    }
                                    echo $print;
                                    ?>
                                      </tbody>
                                    </table>
                                      <?php
                                     
                                      ?>


                                  </div>    </div>    
                                   <?php # end of column ?>  
                                  </div> <?php # end of column ?>
                                  </div> <?php # end of column ?>
                                  <?php
                                  /*<div class="col-lg-8">  <div class="card">
                                    <div class="header">


                                  
                                      <h4 class="title">Edit Profile</h4>
                                    </div>
                                    <div class="content">  <p><h2>Hello World</h2></p></div></div></div> <?php # end of column ?>


                                    # THIS WAS THE ORIGINAL BOXES ABOVE THE SERVER INFO
                                                          <div class="row">
                      <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-user"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="numbers">
                                            <p>Users</p><small>
                                            <?php echo $cctvountu.'/'.$iuc; ?></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-timer"></i> Since the beginning
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-server"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p>Servers</p>
                                            <?php echo "$cctvountRunning / $cctvountAll"; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        Running / Total
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><small>Est Revenue</small></p>
                                            $<?php 
                                            # Total value of available credits, minus those used by the server
                                            $m1 = $lvl1 * 0.10;
                                            $m2 = $lvl2 * 0.20;
                                            $m3 = $lvl3 * 0.30;
                                            $totalcostestimate = $m1+$m2+$m3;
                                            $fn = ($cctv * 0.5) - $totalcostestimate;
                                            echo number_format($fn, 2); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-calendar"></i> $<?php echo number_format($totalcostestimate, 2); ?> server costs
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-wallet"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="numbers">
                                            <p><small>Avail Creds</small></p>
                                            <b><?php echo number_format($cctv); ?></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-wallet"></i> $<?php echo $cctv * 0.50; ?> total value
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                                    */?>