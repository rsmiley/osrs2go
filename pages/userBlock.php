<?php
if($user->isLoggedIn()) {
  # We want to grab their name and latest profile picture from Facebook.
  # Fill the bubble face.
    // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/me?fields=first_name,last_name,picture', $_SESSION['fb_access_token']);
  $ua = $response->getDecodedBody();
  $userAvatar = $ua['picture']['data']['url'];
  if($ua['picture']['data']['is_silhouette']) {
    $userAvatar = 'http://i.imgur.com/x7asaZf.png'; # Default Bubble
  }
  $userName = $ua['first_name'].' '.$ua['last_name'];
  $me = $user->getCurrentUser();
  $rank = $user->getTextRank($_SESSION['userid']);
  $r = $servers->getUserRunning($_SESSION['userid']);
  $run = json_decode($r, true);
  $count = 0;
  if(count($run) != 0) {
  $runData .= "<b><u>Currently Active Servers</u></b>
    <table style='text-align: center; width: 100%'>";  
  }         

  foreach($run as $k) {
    $die =  round(($servers->expectedDeath($k['servername']) - time()) / 60);
    $restartServer = "<a href='/restart/?s={$k['quickdns']}' title='Restart Server'><i class='ti-loop'></i></a>";
    #if($user->isAdmin($_SESSION['userid'])) {
    	$dropdown = '<center><div class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Level '.$k['level'].' <i class="caret"></i>
                          </a>
                          <ul class="dropdown-menu">
                          <li><a href="/restart/?s='.$k['quickdns'].'"><small><b>Restart Server</b></small></a></li>
                          <li><a href="/extend/?s='.$k['quickdns'].'"><small><b>Extend Server</b></small></a></li>
                          <li><a href="/common-problems/"><small><b>Help me!</b></small></a></li>
                          
                          </ul>
                    </div></center>';
   # } else {
    #	$dropdown = "Level {$k['level']}";
    #}
    $runData .= "
    <tr><td style='width: 40%'><small><b>{$k['quickdns']}.osrs2go.com </b></small></td> <td style='width: 25%'> $dropdown </td> <td style='width: 55%'><center><small>{$restartServer} <b>{$die}</b> minutes left</small></center></td></tr>";
    $count++;
  }
  $runData .= "</table>";
  if($count == 0) {
    $runData .= "<center><b>No running servers!</b><br />Why not start one up?</center>";
  }
  #$timenow = date('h:i A');
  #$runData .= "<br /><center>It's <b>{$timenow}</b> right now</center>";
  $d = DB::query("SELECT * FROM servers WHERE owner = %i", $_SESSION['userid']);
} else {
  # Show the login link with some text surrounding signing up.
  # original bubble face image: http://i.imgur.com/anzOWde.png
  # BUBBLE IMAGE: http://i.imgur.com/x7asaZf.png
  #$userName = "Sign in using Facebook!";
  $userAvatar = 'https://osrs2go.com/style/osrs2go/assets/img/bubble.png'; # Default Bubble
  $helper = $fb->getRedirectLoginHelper();
  $permissions = ['email']; // Optional permissions
  $loginUrl = $helper->getLoginUrl('https://osrs2go.com/pages/fbProcess.php', $permissions);
  $userName = '<a href="' . htmlspecialchars($loginUrl) . '">Login with Facebook!</a>';
  $rank = 'Sign in to get started!';
  $me['credits'] = 0;
  $d = array(); # Empty array to make it say 0 launches
}
?>

                          <div class="card card-user">
                              <div class="image">

                                  <img src="https://osrs2go.com/style/osrs2go/assets/img/banner.png" alt="..."/>
                                  </div>
                                  <div class="content">
                                  <div class="author">
                                    <img class="avatar border-white" src="<?php echo $userAvatar; ?>" alt="..."/>
                                    <h4 class="title"><?php echo $userName; ?><br />
                                       <a href="#"><small><?php echo $rank; ?></small></a>
                                    </h4>
                                  </div>
                                  <p class="description text-center">
                                  <?php echo $runData; ?>

                                    
                                  </p>
                                  </div>
                                  <hr>
                                  <div class="text-center">
                                  <div class="row">
                                      <div class="col-xs-3 col-xs-offset-1">
                                          <h5><?php echo $me['credits']; ?><br /><small>Credits</small></h5>
                                      </div>
                                      <div class="col-xs-4">
                                          <h5><?php echo number_format($me['credits'] * 3); ?><br /><small>Available Hours</small></h5>
                                      </div>
                                      <div class="col-sm-3">
                                          <h5><?php echo count($d);?><br />
                                          <small>Servers Used</small></h5>
                                      </div>

                                      </div>
                                  </div>
                                  </div>
                                  <div class="span4">
                                    <div class="card">

                                                    <div class="header">
                                                    <h4 class="title">Start a Server</h4>
                                                </div>
                                                <div class="content">
                                  <form method='post' action='/new-server/' style='display: inline;'>

                                              <select id="server" name="server" class="form-control">
                                  <option value="1">Level 1 (1 credit)</option>
                                  <option value="2">Level 2 (2 credits)</option>
                                  <option value="3">Level 3 (3 credits)</option>
                                  <!-- <option disabled value="4">Always On (See Next Page)</option> -->
                                    </select><br />Select the closest location to you!
                                  <select name='zone' class='form-control' id='zone'>

                                  <?php
                                  foreach($servers->zoneCheck() as $k => $va) {
                                    if($va >= 1) {
                                      $c = DB::queryFirstRow("SELECT * FROM zonelimits WHERE zone = %s", $k);
                                      echo "<option value='{$c['id']}'>{$c['fullname']}</option>
                                      ";
                                    }
                                  }
                                  ?>
                                  </select>
                                  <button id="startsvr" onClick="this.form.submit(); this.disabled=true; this.value='Starting..'; " name="startsvr" class="btn btn-success pull-right">Start Server</button>
                                  </fieldset>
                                  </form>
                                  <br /><br />

                                            </div>
                                  </div>
                                  </div>
                                  </div>      

<?php
if($user->isLoggedIn() && $user->isAdmin($_SESSION['userid'])) {
  # Admin Overview
  
  # Users
  $userCount = count($user->getAllUsers());
  # Servers
  $serverRunningCount = count(json_decode($servers->getRunning(), true));
  $serverTotalCount = count(json_decode($servers->getAllServers(), true));
  # Tickets
  $openCountsql = DB::query("SELECT * FROM tickets WHERE status = 1");
  $openCount = count($openCountsql);
  $closedCountsql = DB::query("SELECT * FROM tickets WHERE status = 2");
  $closedCount = count($closedCountsql);
  $otherCountsql = DB::query("SELECT * FROM tickets WHERE status != 1 AND status != 2");
  $otherCount = count($otherCountsql);
  # Money
  $getTotalValue = DB::query("SELECT * FROM paypalTransLog");
  $displayMoney = 0;
  $transCount = 0;
  foreach($getTotalValue as $dollar) {
    $displayMoney = $displayMoney + $dollar['amount'];
    $transCount++;
  }
  $displayMoney = number_format($displayMoney, 2);

  print "<div class='col-lg-8'>
                      <div class='row'>
                      <div class='col-lg-3 col-sm-6'>
                        <div class='card'>
                            <div class='content'>
                                <div class='row'>
                                    <div class='col-xs-4'>
                                        <div class='icon-big icon-danger text-center'>
                                            <i class='ti-user'></i>
                                        </div>
                                    </div>
                                    <div class='col-xs-8'>
                                        <div class='numbers'>
                                            <p>Users</p><small>
                                            $userCount</small>
                                        </div>
                                    </div>
                                </div>
                                <div class='footer'>
                                    <hr />
                                    <div class='stats' style='text-align:center;'>
                                        Since March 17, 2017
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3 col-sm-6'>
                        <div class='card'>
                            <div class='content'>
                                <div class='row'>
                                    <div class='col-xs-3'>
                                        <div class='icon-big icon-warning text-center'>
                                            <i class='ti-server'></i>
                                        </div>
                                    </div>
                                    <div class='col-xs-9'>
                                        <div class='numbers'>
                                            <p>Servers</p>
                                            $serverRunningCount / $serverTotalCount
                                        </div>
                                    </div>
                                </div>
                                <div class='footer'>
                                    <hr />
                                    <div class='stats' style='text-align:center;'>
                                        Running / Total
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3 col-sm-6'>
                        <div class='card'>
                            <div class='content'>
                                <div class='row'>
                                    <div class='col-xs-3'>
                                        <div class='icon-big icon-success text-center'>
                                            <i class='ti-comment-alt'></i>
                                        </div>
                                    </div>
                                    <div class='col-xs-9'>
                                        <div class='numbers'>
                                            <p><small>Tickets</small></p>
                                            $openCount / $otherCount / $closedCount
                                        </div>
                                    </div>
                                </div>
                                <div class='footer'>
                                    <hr />
                                    <div class='stats' style='text-align:center;'>
                                        Open / Other / Closed
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-3 col-sm-6'>
                        <div class='card'>
                            <div class='content'>
                                <div class='row'>
                                    <div class='col-xs-3'>
                                        <div class='icon-big icon-success text-center'>
                                            <i class='ti-credit-card'></i>
                                        </div>
                                    </div>
                                    <div class='col-xs-9'>
                                        <div class='numbers' style='text-align:center;'>
                                            <p><small>Total Sales</small></p>
                                            <b>$$displayMoney</b>
                                        </div>
                                    </div>
                                </div>
                                <div class='footer'>
                                    <hr />
                                    <div class='stats'>
                                        <b style='color: green;'>$transCount</b> Total Transactions 
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>";
}
?>