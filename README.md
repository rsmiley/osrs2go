# osrs2go
osrs2go was once a service that allowed you to launch a Windows server using Google Cloud to play Oldschool Runescape via Remote Desktop. The script corraborated with Google Cloud, Cloudflare, and Facebook to authenticate and automate the process for users. Users accounts are managed via Facebook to prevent any information stored but their name on osrs2go. This ensured that users did not manage an additional password when using the service.

### Script Requirements
- PHP 5.6/7.x
- A MySQL Database
- Composer
- Updating of hardlinked app tokens/URLs
-- Please [click here](https://gitlab.com/rsmiley/osrs2go/wikis/Files-to-Update) for the list
 - Google Cloud account (to launch servers)
 - Facebook Developer Account (to create application)
 - Cloudflare account (for URL management)
 - Mailgun account (for email sending)
 - PayPal account (for receiving payments)

### How it works
 - User requests a server
 - Google Cloud CLI runs to start a server while the script sleeps for it to start
 - Script polls for the server info
 -- If it failed to issue an IP address (known issue) - kill it and refund
 - A subdomain is created via Cloudflare API for the user pointed to the server IP
 - Server runs for 3 hours (if not extended)
 -- Windows Image setup to auto-shutdown after 3 hours too
 - On next website pageload, cron runs destroying the server
 - User can purchase a new one

